const { FighterRepository } = require("../repositories/fighterRepository");

class FighterService {
  // TODO: Implement methods to work with fighters
  getAll() {}

  create(data) {}

  update(id, data) {}

  delete(id) {}

  search(search) {}
}

module.exports = new FighterService();
